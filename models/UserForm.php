<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * UserForm is the model behind the user form.
 */
class UserForm extends User
{

    public $password_field;
    public $password_confirm_field;
    public $current_password;

    /**
     *
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['username'], 'unique', 'on' => 'create', 'targetClass' => '\app\models\User', 'message' => 'Este usuário já existe.'],
            [['username'], 'unique', 'on' => 'update', 'when' => function ($model) {
                    return $model->isAttributeChanged('username');
                }, 'targetClass' => '\app\models\User', 'message' => 'Este usuário já existe.'],
            [['username'], 'string', 'min' => 2, 'max' => 255],
            [['username'], 'match', 'pattern' => '/^[a-z]\w*$/i'],
            [['password_field'], 'required', 'on' => 'create'],
            [['password_field'], 'required', 'on' => 'password-update-own'],
            [['password_field'], 'string', 'min' => 6, 'max' => 32, 'on' => 'create'],
            [['password_field'], 'string', 'min' => 6, 'max' => 32, 'on' => 'password-update-own'],
            [['password_confirm_field'], 'required', 'on' => 'create'],
            [['password_confirm_field'], 'required', 'on' => 'password-update-own'],
            [['password_confirm_field'], 'string', 'min' => 6, 'max' => 32, 'on' => 'create'],
            [['password_confirm_field'], 'string', 'min' => 6, 'max' => 32, 'on' => 'password-update-own'],
            [['current_password'], 'required', 'on' => 'password-update-own'],
            ['current_password', 'validateCurrentPassword', 'on' => 'password-update-own'],
            [['current_password'], 'string', 'min' => 6, 'max' => 32, 'on' => 'password-update-own'],
            [['current_password'], 'string', 'min' => 6, 'max' => 32],
            [['email'], 'required'],
            [['email'], 'unique', 'on' => 'create', 'targetClass' => '\app\models\User', 'message' => 'Este e-mail já foi usado.'],
            [['email'], 'unique', 'on' => 'update', 'when' => function ($model) {
                    return $model->isAttributeChanged('email');
                }, 'targetClass' => '\app\models\User', 'message' => 'Este e-mail já foi usado.'],
            [['email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['email'], 'trim'],
            [['name'], 'required'],
            [['name'], 'string', 'min' => 2, 'max' => 255],
            [['name'], 'trim'],
            [['password_field'], 'compare', 'compareAttribute' => 'username', 'operator' => '!='],
            [['password_confirm_field'], 'compare', 'compareAttribute' => 'password_field', 'skipOnEmpty' => false]
        ];
    }

    /**
     *
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Usuário',
            'password_hash' => 'Hash de senha',
            'password_reset_token' => 'Token de recuperação de senha',
            'auth_key' => 'Chave de autenticação',
            'email' => 'E-mail',
            'name' => 'Nome',
            'password_field' => 'Senha',
            'password_confirm_field' => 'Confirmação de senha',
            'current_password' => 'Senha atual'
        ];
    }

    /**
     *
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->setPassword($this->password_field);
            $this->generateAuthKey();
            return true;
        } else if (!empty($this->password_field)) {
            $this->setPassword($this->password_field);
            return true;
        }
        return false;
    }

    /**
     * Validates the current password.
     * This method serves as the inline validation for password.
     */
    public function validateCurrentPassword($attribute)
    {
        if (!$this->validatePassword($this->$attribute)) {
            $this->addError($attribute, 'Senha atual inválida');
        }
        /*
         * if (!$this->hasErrors()) {
         * $user = User::findByUsername($this->username);
         * if (!$user || !$user->validatePassword($this->current_password)) {
         * $this->addError($attribute, 'Senha inválida.');
         * }
         * }
         */
    }
}
