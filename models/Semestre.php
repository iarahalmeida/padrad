<?php

namespace app\models;

use Yii;
use \nepstor\validators\DateTimeCompareValidator;

/**
 * This is the model class for table "semestre".
 *
 * @property int $id
 * @property string $ano_numero
 * @property string $data_inicio
 * @property string $data_termino
 * @property string $data_inicio_envio_pad
 * @property string $data_termino_envio_pad
 * @property string $data_inicio_avaliacao_pad
 * @property string $data_termino_avaliacao_pad
 * @property string $data_inicio_correcao_pad
 * @property string $data_termino_correcao_pad
 * @property string $data_inicio_envio_rad
 * @property string $data_termino_envio_rad
 */
class Semestre extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'semestre';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ano_numero', 'data_inicio', 'data_termino', 'data_inicio_envio_pad', 'data_termino_envio_pad', 'data_inicio_avaliacao_pad', 'data_termino_avaliacao_pad', 'data_inicio_correcao_pad', 'data_termino_correcao_pad', 'data_inicio_envio_rad', 'data_termino_envio_rad'], 'required'],
            [['data_inicio', 'data_termino', 'data_inicio_envio_pad', 'data_termino_envio_pad', 'data_inicio_avaliacao_pad', 'data_termino_avaliacao_pad', 'data_inicio_correcao_pad', 'data_termino_correcao_pad', 'data_inicio_envio_rad', 'data_termino_envio_rad'], 'safe'],
            [['ano_numero'], 'string', 'max' => 255],
            ['data_termino', DateTimeCompareValidator::className(), 'message' => 'Intervalo Negativo', 'compareAttribute' => 'data_inicio', 'operator' => '>='],
            ['data_inicio_envio_pad', DateTimeCompareValidator::className(), 'message' => 'Intervalo Negativo', 'compareAttribute' => 'data_inicio', 'operator' => '>='],
            ['data_termino_envio_pad', DateTimeCompareValidator::className(), 'message' => 'Intervalo Negativo', 'compareAttribute' => 'data_inicio_envio_pad', 'operator' => '>='],
            ['data_inicio_avaliacao_pad', DateTimeCompareValidator::className(), 'message' => 'Intervalo Negativo', 'compareAttribute' => 'data_termino_envio_pad', 'operator' => '>='],
            ['data_termino_avaliacao_pad', DateTimeCompareValidator::className(), 'message' => 'Intervalo Negativo', 'compareAttribute' => 'data_inicio_avaliacao_pad', 'operator' => '>='],
            ['data_inicio_correcao_pad', DateTimeCompareValidator::className(), 'message' => 'Intervalo Negativo', 'compareAttribute' => 'data_termino_avaliacao_pad', 'operator' => '>='],
            ['data_termino_correcao_pad', DateTimeCompareValidator::className(), 'message' => 'Intervalo Negativo', 'compareAttribute' => 'data_inicio_correcao_pad', 'operator' => '>='],
            ['data_inicio_envio_rad', DateTimeCompareValidator::className(), 'message' => 'Intervalo Negativo', 'compareAttribute' => 'data_termino_correcao_pad', 'operator' => '>='],
            ['data_termino_envio_rad', DateTimeCompareValidator::className(), 'message' => 'Intervalo Negativo', 'compareAttribute' => 'data_inicio_envio_rad', 'operator' => '>='],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ano_numero' => 'Ano/Número',
            'data_inicio' => 'Data de início do semestre',
            'data_termino' => 'Data de término do semestre',
            'data_inicio_envio_pad' => 'Data de início para envio do PAD',
            'data_termino_envio_pad' => 'Data de término para envio do PAD',
            'data_inicio_avaliacao_pad' => 'Data de início para avaliação do PAD',
            'data_termino_avaliacao_pad' => 'Data de término para avaliação do PAD',
            'data_inicio_correcao_pad' => 'Data de início para correção do PAD',
            'data_termino_correcao_pad' => 'Data de término para correção do PAD',
            'data_inicio_envio_rad' => 'Data de início para envio do RAD',
            'data_termino_envio_rad' => 'Data de término para envio do RAD',
        ];
    }
    
    public function attributeComments()
    {
        return [
            'id' => 'ID',
            'ano_numero' => 'Ano e número do semestre',
            'data_inicio' => 'Data de início do semestre',
            'data_termino' => 'Data de término do semestre',
            'data_inicio_envio_pad' => 'Data de início para envio do PAD',
            'data_termino_envio_pad' => 'Data de término para envio do PAD',
            'data_inicio_avaliacao_pad' => 'Data de início para avaliação do PAD',
            'data_termino_avaliacao_pad' => 'Data de término para avaliação do PAD',
            'data_inicio_correcao_pad' => 'Data de início para correção do PAD',
            'data_termino_correcao_pad' => 'Data de término para correção do PAD',
            'data_inicio_envio_rad' => 'Data de início para envio do RAD',
            'data_termino_envio_rad' => 'Data de término para envio do RAD',
        ];
    }
}
