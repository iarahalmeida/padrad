<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detalhesatividade".
 *
 * @property int $id
 * @property string $nome
 * @property string $tipo 
 * @property int $carga_horaria_maxima
 * @property int $carga_horaria_minima
 * @property Atividadeensino[] $atividadeensinos
 * @property Atividadeextensao[] $atividadeextensaos
 * @property Atividadegestao[] $atividadegestaos
 * @property Atividadepesquisa[] $atividadepesquisas
 */
class DetalhesAtividade extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detalhesatividade';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'tipo', 'carga_horaria_maxima', 'carga_horaria_minima'], 'required'],
            [['carga_horaria_maxima', 'carga_horaria_minima'], 'integer', 'min' =>0],
            ['carga_horaria_maxima', 'compare', 'compareAttribute' => 'carga_horaria_minima','operator' => '>=', 'type' => 'number'],
            [['nome'], 'string', 'max' => 255],
            [['tipo'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'tipo' => 'Tipo de Atividade',
            'carga_horaria_maxima' => 'Carga Horária Máxima',
            'carga_horaria_minima' => 'Carga Horária Mínima',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtividadeensinos()
    {
        return $this->hasMany(Atividadeensino::className(), ['id_detalhesatividade' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtividadeextensaos()
    {
        return $this->hasMany(Atividadeextensao::className(), ['id_detalhesatividade' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtividadegestaos()
    {
        return $this->hasMany(Atividadegestao::className(), ['id_detalhesatividade' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtividadepesquisas()
    {
        return $this->hasMany(Atividadepesquisa::className(), ['id_detalhesatividade' => 'id']);
    }
}
