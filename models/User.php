<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $email
 * @property string $name
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $password_field;
    public $password_confirm_field;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [         
            [['username'], 'required'],
            [['username'], 'unique', 'targetClass' => '\app\models\User', 'message' => 'Este usuário já existe.'],
            [['username'], 'string', 'min' => 2, 'max' => 255],
            [['username'], 'match', 'pattern' => '/^[a-z]\w*$/i'],
            [['username'], 'trim'],
            [['password_hash'], 'required'],
            [['password_hash'], 'string', 'max' => 255],
            [['password_reset_token'], 'unique'],
            [['password_reset_token'], 'string', 'max' => 255],
            [['auth_key'], 'required'],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'required'],
            [['email'], 'unique', 'targetClass' => '\app\models\User', 'message' => 'Este e-mail já foi usado.'],
            [['email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['email'], 'trim'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'trim'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Usuário',
            'password_hash' => 'Hash de senha',
            'password_reset_token' => 'Token de recuperação',
            'auth_key' => 'Chave de autenticação',
            'email' => 'E-mail',
            'name' => 'Nome',
            'password_field' => 'Senha',
            'password_confirm_field' => 'Confirmação de senha',
        ];
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \yii\web\IdentityInterface::findIdentity()
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     *
     * {@inheritdoc}
     * @see \yii\web\IdentityInterface::findIdentityByAccessToken()
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne([
                'username' => $username
        ]);
    }

    /**
     *
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     *
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     *
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password
     *        	password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates a hash to a given password and change it's model value
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Allow the option 'remember me', generating an authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
}
