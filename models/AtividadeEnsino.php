<?php

namespace app\models;

use Yii;
use \nepstor\validators\DateTimeCompareValidator;

/**
 * This is the model class for table "atividadeensino".
 *
 * @property int $id
 * @property string $nome
 * @property int $carga_horaria
 * @property string $data_inicio
 * @property string $data_fim
 * @property string $tipo
 * @property int $id_detalhesatividade
 *
 * @property Detalhesatividade $detalhesatividade
 */
class AtividadeEnsino extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atividadeensino';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'carga_horaria', 'id_detalhesatividade'], 'required'],
            [['carga_horaria', 'id_detalhesatividade'], 'integer'],
            [['data_inicio', 'data_fim'], 'safe'],
            [['nome', 'tipo'], 'string', 'max' => 255],
            [['carga_horaria'], 'compare', 'compareValue' => 0, 'operator' => '>=', 'type' => 'number'],
            ['carga_horaria', 'validateCargaHorariaMinima'],
            ['carga_horaria', 'validateCargaHorariaMaxima'],
            ['data_fim', DateTimeCompareValidator::className(), 'message' => 'Intervalo de tempo negativo', 'compareAttribute' => 'data_inicio', 'operator' => '>='],
            [['id_detalhesatividade'], 'exist', 'skipOnError' => true, 'targetClass' => DetalhesAtividade::className(), 'targetAttribute' => ['id_detalhesatividade' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'carga_horaria' => 'Carga Horaria',
            'data_inicio' => 'Data Inicio',
            'data_fim' => 'Data Fim',
            'tipo' => 'Tipo',
            'id_detalhesatividade' => 'Id Detalhesatividade',
        ];
    }
    
     public function attributeComments()
     {
         return [
            'id' => 'ID',
            'nome' => 'Nome da Atividade',
            'carga_horaria' => 'Carga Horária Semanal da Atividade',
            'data_inicio' => 'Data Início da Atividade',
            'data_fim' => 'Data de Término da Atividade',
            'tipo' => 'Tipo',
            'id_detalhesatividade' => 'Id DetalhesAtividade',
         ];         
     }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalhesatividade()
    {
        return $this->hasOne(Detalhesatividade::className(), ['id' => 'id_detalhesatividade']);
    }
    
    public function validateCargaHorariaMinima($attribute)
    {
        if ($this->detalhesatividade->carga_horaria_minima >= $this->$attribute) {
            $this->addError($attribute, 'Carga horária mínima deve ser no minimo ' . $this->detalhesatividade->carga_horaria_minima);
        }
    }
    
    public function validateCargaHorariaMaxima($attribute)
    {
        if ($this->detalhesatividade->carga_horaria_maxima <= $this->$attribute) {
            $this->addError($attribute, 'Carga horária mínima deve ser no máximo ' . $this->detalhesatividade->carga_horaria_maxima);
        }
    }
}
