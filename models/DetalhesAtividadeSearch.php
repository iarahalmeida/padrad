<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DetalhesAtividade;

/**
 * DetalhesAtividadeSearch represents the model behind the search form of `app\models\DetalhesAtividade`.
 */
class DetalhesAtividadeSearch extends DetalhesAtividade
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'carga_horaria_maxima', 'carga_horaria_minima'], 'integer'],
            [['nome'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DetalhesAtividade::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'carga_horaria_maxima' => $this->carga_horaria_maxima,
            'carga_horaria_minima' => $this->carga_horaria_minima,
        ]);

        $query->andFilterWhere(['like', 'nome', $this->nome]);

        return $dataProvider;
    }
}
