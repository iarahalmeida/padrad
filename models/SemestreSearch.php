<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Semestre;

/**
 * SemestreSearch represents the model behind the search form of `app\models\Semestre`.
 */
class SemestreSearch extends Semestre
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['ano_numero', 'data_inicio', 'data_termino', 'data_inicio_envio_pad', 'data_termino_envio_pad', 'data_inicio_avaliacao_pad', 'data_termino_avaliacao_pad', 'data_inicio_correcao_pad', 'data_termino_correcao_pad', 'data_inicio_envio_rad', 'data_termino_envio_rad'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Semestre::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'data_inicio' => $this->data_inicio,
            'data_termino' => $this->data_termino,
            'data_inicio_envio_pad' => $this->data_inicio_envio_pad,
            'data_termino_envio_pad' => $this->data_termino_envio_pad,
            'data_inicio_avaliacao_pad' => $this->data_inicio_avaliacao_pad,
            'data_termino_avaliacao_pad' => $this->data_termino_avaliacao_pad,
            'data_inicio_correcao_pad' => $this->data_inicio_correcao_pad,
            'data_termino_correcao_pad' => $this->data_termino_correcao_pad,
            'data_inicio_envio_rad' => $this->data_inicio_envio_rad,
            'data_termino_envio_rad' => $this->data_termino_envio_rad,
        ]);

        $query->andFilterWhere(['like', 'ano_numero', $this->ano_numero]);

        return $dataProvider;
    }
}
