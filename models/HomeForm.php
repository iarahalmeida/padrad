<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * HomeForm is the model behind the home form.
 */
class HomeForm extends \yii\db\ActiveRecord
{
    public $semester_id;
    public $professor_name;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['semester_id'], 'required'],
            [['semester_id'], 'exist', 'skipOnError' => true, 'targetClass' => Semestre::className(), 'targetAttribute' => ['semester_id' => 'id']],
            [['professor_name'], 'required'],
            [['professor_name'], 'trim'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'semester_id' => 'Semestre',
            'professor_name' => 'Nome do docente',
        ];
    }
}
