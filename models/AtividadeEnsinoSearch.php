<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AtividadeEnsino;

/**
 * AtividadeEnsinoSearch represents the model behind the search form of `app\models\AtividadeEnsino`.
 */
class AtividadeEnsinoSearch extends AtividadeEnsino
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'carga_horaria', 'id_detalhesatividade'], 'integer'],
            [['nome', 'data_inicio', 'data_fim'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AtividadeEnsino::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'carga_horaria' => $this->carga_horaria,
            'data_inicio' => $this->data_inicio,
            'data_fim' => $this->data_fim,
            'id_detalhesatividade' => $this->id_detalhesatividade,
        ]);

        $query->andFilterWhere(['like', 'nome', $this->nome]);

        return $dataProvider;
    }
}
