<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "disciplina".
 *
 * @property int $id
 * @property string $nome
 * @property string $periodo
 * @property int $carga_horaria
 */
class Disciplina extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'disciplina';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'periodo', 'carga_horaria'], 'required'],
            [['carga_horaria'], 'integer'],
            [['nome', 'periodo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'periodo' => 'Periodo',
            'carga_horaria' => 'Carga Horaria',
        ];
    }
}
