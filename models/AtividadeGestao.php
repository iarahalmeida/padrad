<?php

namespace app\models;

use Yii;
use \nepstor\validators\DateTimeCompareValidator;

/**
 * This is the model class for table "atividadegestao".
 *
 * @property int $id
 * @property string $nome
 * @property int $carga_horaria
 * @property string $data_inicio
 * @property string $data_fim
 * @property string $numero_portaria
 * @property int $id_detalhesatividade
 *
 * @property Detalhesatividade $detalhesatividade
 */
class AtividadeGestao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atividadegestao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'carga_horaria', 'numero_portaria', 'id_detalhesatividade'], 'required'],
            [['carga_horaria', 'id_detalhesatividade'], 'integer'],
            [['data_inicio', 'data_fim'], 'safe'],
            [['nome', 'numero_portaria'], 'string', 'max' => 255],
            ['data_fim', DateTimeCompareValidator::className(), 'message' => 'Intervalo de tempo negativo', 'compareAttribute' => 'data_inicio', 'operator' => '>='],
            [['id_detalhesatividade'], 'exist', 'skipOnError' => true, 'targetClass' => DetalhesAtividade::className(), 'targetAttribute' => ['id_detalhesatividade' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'carga_horaria' => 'Carga Horaria',
            'data_inicio' => 'Data Inicio',
            'data_fim' => 'Data Fim',
            'numero_portaria' => 'Numero Portaria',
            'id_detalhesatividade' => 'Id Detalhesatividade',
        ];
    }
    
    public function attributeComments()
     {
         return [
            'id' => 'ID',
            'nome' => 'Nome da Atividade',
            'carga_horaria' => 'Carga Horária Semanal da Atividade',
            'data_inicio' => 'Data Início da Atividade',
            'data_fim' => 'Data de Término da Atividade',
            'tipo' => 'Tipo',
            'id_detalhesatividade' => 'Id DetalhesAtividade',
         ];         
     }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalhesatividade()
    {
        return $this->hasOne(Detalhesatividade::className(), ['id' => 'id_detalhesatividade']);
    }
}
