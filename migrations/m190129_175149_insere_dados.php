<?php

use yii\db\Migration;
use app\models\UserForm;

/**
 * Class m190129_175149_insere_dados
 */
class m190129_175149_insere_dados extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB';
        }

        if ($this->insertUser($tableOptions)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function insertUser($options)
    {
        $user = new UserForm();
        $user->scenario = 'create';
        $user->username = 'admin';
        $user->email = 'admin@admin.com';
        $user->password_field = 'admin123';
        $user->password_confirm_field = 'admin123';
        $user->setPassword($user->password_field);
        $user->name = 'Admin PAD/RAD';
        $user->generateAuthKey();
        if ($user->save()) {
            echo "Usuário admin inserido com sucesso\n";
            return true;
        } else {
            var_dump($user->getErrors());
            echo "Erro ao inserir admin\n";
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190129_175149_insere_dados cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190129_175149_insere_dados cannot be reverted.\n";

        return false;
    }
    */
}
