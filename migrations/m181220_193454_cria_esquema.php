<?php

use yii\db\Migration;

/**
 * Class m181220_193454_cria_esquema
 */
class m181220_193454_cria_esquema extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \Yii::$app->runAction('migrate', ['migrationPath' => '@yii/rbac/migrations', 'interactive' => false]);

        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB';
        }

        $this->createUser($tableOptions);
        $this->createSemestre($tableOptions);
        $this->createDetalhesAtividade($tableOptions);
        $this->createAtividadeEnsino($tableOptions);
        $this->createAtividadePesquisa($tableOptions);
        $this->createAtividadeExtensao($tableOptions);
        $this->createAtividadeGestao($tableOptions);
        $this->createDocente($tableOptions);
        $this->createCurso($tableOptions);
        $this->createDisciplina($tableOptions);
        $this->createAula($tableOptions);
        $this->createPAD($tableOptions);
        $this->createAtividadeEnsinoPad($tableOptions);
        $this->createAtividadeExtensaoPad($tableOptions);
        $this->createAtividadeGestaoPad($tableOptions);
        $this->createAtividadePesquisaPad($tableOptions);
    }

    public function createUser($options)
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'name' => $this->string()->notNull(),
            ], $options);
    }
    
    public function createSemestre($options)
    {
        $this->createTable('semestre', [
            'id' => $this->primaryKey(),
            'ano_numero' => $this->string()->notNull(),
            'data_inicio' => $this->date()->notNull(),
            'data_termino' => $this->date()->notNull(),
            'data_inicio_envio_pad' => $this->date()->notNull(),
            'data_termino_envio_pad' => $this->date()->notNull(),
            'data_inicio_avaliacao_pad' => $this->date()->notNull(),
            'data_termino_avaliacao_pad' => $this->date()->notNull(),
            'data_inicio_correcao_pad' => $this->date()->notNull(),
            'data_termino_correcao_pad' => $this->date()->notNull(),
            'data_inicio_envio_rad' => $this->date()->notNull(),
            'data_termino_envio_rad' => $this->date()->notNull(),
        ], $options);
    }
    
    public function createDetalhesAtividade($options)
    {
        $this->createTable('detalhesatividade', [
            'id' => $this->primaryKey(),
            'nome' => $this->string()->notNull(),
            'tipo' => $this->string()->notNull(),
            'carga_horaria_maxima' => $this->integer()->notNull(),
            'carga_horaria_minima' => $this->integer()->notNull(),
        ], $options);
    }
    
    public function createAtividadeEnsino($options)
    {
        $this->createTable('atividadeensino', [
            'id' => $this->primaryKey(),
            'nome' => $this->string()->notNull(),
            'carga_horaria' => $this->integer()->notNull(),
            'data_inicio' => $this->date(),
            'data_fim' => $this->date(),
            'id_detalhesatividade' => $this->integer()->notNull(),
        ], $options);
        
        $this->addForeignKey(
            'fk-atividadeensino-detalhesatividade_id',
            'atividadeensino',
            'id_detalhesatividade',
            'detalhesatividade',
            'id',
            'CASCADE'
        );

    }
    
    public function createAtividadePesquisa($options)
    {
        $this->createTable('atividadepesquisa', [
            'id' => $this->primaryKey(),
            'nome' => $this->string()->notNull(),
            'carga_horaria' => $this->integer()->notNull(),
            'data_inicio' => $this->date(),
            'data_fim' => $this->date(),
            'arquivo' => $this->string(),
            'id_detalhesatividade' => $this->integer()->notNull(),
        ], $options);
        
        $this->addForeignKey(
            'fk-atividadepesquisa-detalhesatividade_id',
            'atividadepesquisa',
            'id_detalhesatividade',
            'detalhesatividade',
            'id',
            'CASCADE'
        );
    }
    
    public function createAtividadeExtensao($options)
    {
        $this->createTable('atividadeextensao', [
            'id' => $this->primaryKey(),
            'nome' => $this->string()->notNull(),
            'carga_horaria' => $this->integer()->notNull(),
            'data_inicio' => $this->date(),
            'data_fim' => $this->date(),
            'arquivo' => $this->string(),
            'id_detalhesatividade' => $this->integer()->notNull(),
        ], $options);
        
        $this->addForeignKey(
            'fk-atividadeextensao-detalhesatividade_id',
            'atividadeextensao',
            'id_detalhesatividade',
            'detalhesatividade',
            'id',
            'CASCADE'
        );
    }
    
    public function createAtividadeGestao($options)
    {
        $this->createTable('atividadegestao', [
            'id' => $this->primaryKey(),
            'nome' => $this->string()->notNull(),
            'carga_horaria' => $this->integer()->notNull(),
            'data_inicio' => $this->date(),
            'data_fim' => $this->date(),
            'numero_portaria' => $this->string()->notNull(),
            'id_detalhesatividade' => $this->integer()->notNull(),
        ], $options);
        
        $this->addForeignKey(
            'fk-atividadegestao-detalhesatividade_id',
            'atividadegestao',
            'id_detalhesatividade',
            'detalhesatividade',
            'id',
            'CASCADE'
        );
    }
    
    public function createDocente($options)
    {
        $this->createTable('docente', [
            'id' => $this->primaryKey(),
            'tipo_regime' => $this->string()->notNull(),
        ], $options);
        
        $this->addForeignKey(
            'fk-docente-user_id',
            'docente',
            'id',
            'user',
            'id',
            'CASCADE'
        );
    }
    
    public function createCurso($options)
    {
        $this->createTable('curso', [
            'id' => $this->primaryKey(),
            'nome' => $this->string()->notNull(),
            'tipo' => $this->string()->notNull(),
        ], $options);
    }
    
    public function createDisciplina($options)
    {
        $this->createTable('disciplina', [
            'id' => $this->primaryKey(),
            'nome' => $this->string()->notNull(),
            'periodo' => $this->string()->notNull(),
            'carga_horaria' => $this->integer()->notNull(),
        ], $options);
    }
    
    public function createAula($options)
    {
        $this->createTable('aula', [
            'id' => $this->primaryKey(),
            'id_curso' => $this->integer()->notNull(),
            'id_disciplina' => $this->integer()->notNull(),
            'id_semestre' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
        ], $options);
        
        $this->addForeignKey(
            'fk-aula-curso_id', 
            'aula', 
            'id_curso', 
            'curso', 
            'id',
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk-aula-disciplina_id', 
            'aula',
            'id_disciplina',
            'disciplina', 
            'id', 
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk-aula-user_id', 
            'aula',
            'id_user',
            'user', 
            'id', 
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk-aula-semestre_id', 
            'aula',
            'id_semestre',
            'semestre', 
            'id', 
            'CASCADE'
        );
        
    }
    
    public function createPAD($options)
    {
        $this->createTable('PAD', [
            'id' => $this->primaryKey(),
            'carga-horaria' => $this->integer()->notNull(),
            'envio' => $this->timestamp()->notNull(),
            'id_docente' => $this->integer()->notNull(),
            'id_semestre' => $this->integer()->notNull(),
        ], $options);
        
        $this->addForeignKey(
                'fk-PAD-docente_id',
                'PAD', 
                'id_docente', 
                'user', 
                'id',
                'CASCADE'
        );
        
        $this->addForeignKey(
                'fk-PAD-semestre_id', 
                'PAD', 
                'id_semestre', 
                'semestre', 
                'id',
                'CASCADE'
        );
    }
    
    public function createAtividadeEnsinoPad($options)
    {
        $this->createTable('AtividadeEnsinoPad', [
            'id' => $this->primaryKey(),
            'id_pad' => $this->integer()->notNull(),
            'id_atividadeensino' => $this->integer()->notNull(),
        ], $options);
        
        $this->addForeignKey(
            'fk-atenspad-pad_id', 
            'AtividadeEnsinoPad', 
            'id_pad', 
            'PAD', 
            'id', 
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk-atenspad-atividadeensino_id', 
            'AtividadeEnsinoPad', 
            'id_atividadeensino', 
            'AtividadeEnsino', 
            'id', 
            'CASCADE'
        );
    }
    
    public function createAtividadeExtensaoPad($options)
    {
        $this->createTable('AtividadeExtensaoPad', [
            'id' => $this->primaryKey(),
            'id_pad' => $this->integer()->notNull(),
            'id_atividadeextensao' => $this->integer()->notNull(),
        ], $options);
        
        $this->addForeignKey(
            'fk-atextpad-pad_id',  
            'AtividadeExtensaoPad', 
            'id_pad', 
            'PAD', 
            'id',
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk-atextpad-atividadeextensao_id',  
            'AtividadeExtensaoPad', 
            'id_atividadeextensao', 
            'AtividadeExtensao', 
            'id',
            'CASCADE'
        );
    }
    
    public function createAtividadeGestaoPad($options)
    {
        $this->createTable('AtividadeGestaoPad', [
            'id' => $this->primaryKey(),
            'id_pad' => $this->integer()->notNull(),
            'id_atividadegestao' => $this->integer()->notNull(),
        ], $options);
        
        $this->addForeignKey(
            'fk-atgestpad-pad_id', 
            'AtividadeGestaoPad', 
            'id_pad', 
            'PAD', 
            'id', 
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk-atgestpad-atividadegestao_id', 
            'AtividadeGestaoPad', 
            'id_atividadegestao', 
            'AtividadeGestao', 
            'id', 
            'CASCADE'
        );
    }
    
    public function createAtividadePesquisaPad($options)
    {
        $this->createTable('atividadepesquisapad', [
            'id' => $this->primaryKey(),
            'id_pad' => $this->integer()->notNull(),
            'id_atividadepesquisa' => $this->integer()->notNull(),
        ], $options);
        
        $this->addForeignKey(
            'fk-atpesqpad-pad_id', 
            'AtividadePesquisaPad', 
            'id_pad', 
            'PAD', 
            'id', 
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk-atpesqpad-atividadepesquisa_id', 
            'AtividadePesquisaPad', 
            'id_atividadepesquisa', 
            'AtividadePesquisa', 
            'id', 
            'CASCADE'
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropAtividadeGestao();
        $this->dropAtividadeExtensao();
        $this->dropAtividadePesquisa();
        $this->dropAtividadeEnsino();
        $this->dropAula();
        $this->dropCurso();
        $this->dropDetalhesAtividade();
        $this->dropDisciplina();
        $this->dropDocente();
        $this->dropPAD();
        $this->dropSemestre();
        $this->dropUser();
    }
    
    public function dropUser()
    {
        $this->dropTable('user');
    }
    
    public function dropSemestre()
    {
        $this->dropTable('semestre');
    }
    
    public function dropDetalhesAtividade()
    {
        $this->dropTable('detalhesatividade');
    }
    
    public function dropAtividadeEnsino()
    {
        $this->dropForeignKey(
            'fk-atividadeensino-detalhesatividade_id',
            'atividadeensino'
        );

        $this->dropTable('atividadeensino');
    }
    
    public function dropAtividadePesquisa()
    {
        $this->dropForeignKey(
            'fk-atividadepesquisa-detalhesatividade_id',
            'atividadepesquisa'
        );

        $this->dropTable('atividadepesquisa');
    }
    
    public function dropAtividadeExtensao()
    {
        $this->dropForeignKey(
            'fk-atividadeextensao-detalhesatividade_id',
            'atividadeextensao'
        );

        $this->dropTable('atividadeextensao');
    }
    
    public function dropAtividadeGestao()
    {
        $this->dropForeignKey(
            'fk-atividadegestao-detalhesatividade_id',
            'atividadegestao'
        );

        $this->dropTable('atividadegestao');
    }
    
    public function dropDocente()
    {
        $this->dropForeignKey(
            'fk-docente-user_id',
            'docente'
        );

        $this->dropTable('docente');
    }
    
    public function dropCurso()
    {
        $this->dropTable('curso');
    }
    
    public function dropDisciplina()
    {
        $this->dropTable('disciplina');
    }
    
    public function dropAula()
    {
        $this->dropForeignKey(
            'fk-aula-curso_id', 
            'aula'
        );
        
        $this->dropForeignKey(
            'fk-aula-disciplina_id', 
            'aula'
        );
        
        $this->dropForeignKey(
            'fk-aula-usuario_id', 
            'aula'
        );
        
        $this->dropForeignKey(
            'fk-aula-semestre_id', 
            'aula'
        );
        
        $this->dropTable('aula');
    }
    
    public function dropPAD()
    {
        $this->dropForeignKey(
            'fk-PAD-docente_id', 
            'PAD'
        );
        
        $this->dropForeignKey(
            'fk-PAD-semestre_id', 
            'PAD'
        );
        
        $this->dropTable('PAD');
    }
    
    public function dropAtividadeEnsinoPad()
    {
        $this->dropForeignKey(
            'fk-atenspad-pad_id',
            'AtividadeEnsinoPad'
        );
        
        $this->dropForeignKey(
            'fk-atenspad-atividadeensino_id',
            'AtividadeEnsinoPad'
        );
        
        $this->dropTable('AtividadeEnsinoPad');
    }
    
    public function dropAtividadeExtensaoPad()
    {
        $this->dropForeignKey(
            'fk-atextpad-pad_id',
            'AtividadeExtensaoPad'
        );
        
        $this->dropForeignKey(
            'fk-atextpad-atividadeensino_id',
            'AtividadeExtensaoPad'
        );
        
        $this->dropTable('AtividadeExtensaoPad');
    }
    
    public function dropAtividadeGestaoPad()
    {
        $this->dropForeignKey(
            'fk-atgestpad-pad_id',
            'AtividadeGestaoPad'
        );
        
        $this->dropForeignKey(
            'fk-atgestpad-atividadegestao_id',
            'AtividadeGestaoPad'
        );
        
        $this->dropTable('AtividadeGestaoPad');
    }
    
    public function dropAtividadePesquisaPad()
    {
        $this->dropForeignKey(
            'fk-atpesqpad-pad_id',
            'AtividadePesquisaPad'
        );
        
        $this->dropForeignKey(
            'fk-atpesqpad-atividadepesquisa_id',
            'AtividadePesquisaPad'
        );
        
        $this->dropTable('AtividadePesquisaPad');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181220_193454_cria_esquema cannot be reverted.\n";

        return false;
    }
    */
}
