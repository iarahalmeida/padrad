# PADRAD
Engenharia de Software II
PAD/RAD

## Tutorial de instalação do ambiente de desenvolvimento para o Linux
##### XAMPP, Composer e Yii2

### XAMPP
1. Baixe o XAMPP para o Linux na versão desejada a partir do seguinte link: https://www.apachefriends.org/pt_br/download.html

> Sugestão: versão 7.2.13 com PHP 7.2.13

2. O resultado do download é um script de instalação. Torne-o executável e comece a instalação com os seguintes comandos (substituindo version pela versão do XAMPP baixada):
```
# chmod +x xampp-linux-version-installer.run 
# ./xampp-linux-version-installer.run
```
> A execução da instalação pode precisar de privilégios de super usuário.

Exemplo:
```
# chmod +x xampp-linux-x64-7.2.13-0-installer.run 
# ./xampp-linux-x64-7.2.13-0-installer.run
```
> Sugestão: Next > Next > Next > Desmarque “Learn more about Bitnami for XAMPP” e Next > Next. Após realizar a instalação, deixe marcado “Launch XAMPP” e clique em Finish.

Após a instalação, o XAMPP será instalado em /opt/lampp.

Os arquivos de configuração se encontram nos seguintes destinos:
```
/opt/lampp/etc/httpd.conf - Configuração do Apache.
/opt/lampp/etc/php.ini - Configuração do PHP.
/opt/lampp/phpmyadmin/config.inc.php - Configuração do phpMyAdmin.
/opt/lampp/etc/proftpd.conf - Configuração do proFTP.
/opt/lampp/etc/my.cnf - Configuração do MySQL.
```
Ao abrir o XAMPP, certifique-se de que o Apache e o MySQL Database estejam em execução.

Para verificar se a instalação foi concluída com sucesso, acesse http://localhost:80

Para abrir o gerenciador do XAMPP após a instalação, execute o seguinte comando:
```
# cd /opt/lampp
# sudo ./manager-linux-x64.run &
```
3. Faça o link dos binários do PHP fornecidos pelo XAMPP com o seu PATH:
```
# sudo ln -s /opt/lampp/bin/php /usr/bin/php
# sudo ln -s /opt/lampp/bin/php /usr/local/bin/php
```
Caso já exista uma versão do PHP instalada no sistema, será necessário forçar o link (o que pode ser feito trocando -s por -sf nos dois comandos acima).
> Atenção: para o XAMPP funcionar corretamente, certifique-se de que o pacote que contém o netstat esteja instalado na sua distribuição (normalmente net-tools e inetutils).