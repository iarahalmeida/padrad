<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Alterar: ' . $model->name . ' (' . $model->username . ')';
$this->params['breadcrumbs'][] = ['label' => 'Usuários', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name . ' (' . $model->username . ')', 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Alterar';
?>
<div class="user-update">

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-body">
            <div class="user-form">

                <?php
                $form = ActiveForm::begin([
                            'id' => 'user-form',
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => true,
                                //'validateOnChange' => true,
                ]);
                ?>

                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'password_field')->passwordInput()->label('Nova senha (opcional)') ?>

                <?= $form->field($model, 'password_confirm_field')->passwordInput()->label('Confirmação de nova senha (opcional)') ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

</div>
