<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Criar usuário';
$this->params['breadcrumbs'][] = ['label' => 'Usuários', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-body">
            <div class="user-form">

                <?php
                $form = ActiveForm::begin([
                            'id' => 'user-create-form',
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => true,
                                //'validateOnChange' => true,
                ]);
                ?>

                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'password_field')->passwordInput() ?>

                <?= $form->field($model, 'password_confirm_field')->passwordInput() ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

</div>
