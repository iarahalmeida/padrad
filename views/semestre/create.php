<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Semestre */

$this->title = 'Criar semestre';
$this->params['breadcrumbs'][] = ['label' => 'Semestres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="semestre-create">
    <!-- Default box -->
    <div class="box box-success">
        <div class="box-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
        <!-- /.box-body -->
        </div>
    <!-- /.box -->
    </div>
</div>
