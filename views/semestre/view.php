<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Semestre */

$this->title = $model->ano_numero;
$this->params['breadcrumbs'][] = ['label' => 'Semestres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="semestre-view">
    <!-- Default box -->
    <div class="box box-success">
        <div class="box-body">
    <p>
        <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Excluir', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Você tem certeza que deseja excluir este item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ano_numero',
            'data_inicio',
            'data_termino',
            'data_inicio_envio_pad',
            'data_termino_envio_pad',
            'data_inicio_avaliacao_pad',
            'data_termino_avaliacao_pad',
            'data_inicio_correcao_pad',
            'data_termino_correcao_pad',
            'data_inicio_envio_rad',
            'data_termino_envio_rad',
        ],
    ]) ?>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
