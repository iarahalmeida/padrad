<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Semestre */
/* @var $form yii\widgets\ActiveForm */

$comments = $model->attributeComments();

?>

<div class="semestre-form">

    <?php $form = ActiveForm::begin(); ?>   
        
        <!--Semestre-->
        <h2 class="page-header"><i class="fa fa-edit"></i> Semestre</h2>
        <div class="form-group col-xs-12 col-lg-12">

            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?=
                $form->field($model, 'ano_numero')->textInput(['placeholder' => $comments['ano_numero']])
                ?>
            </div>

        </div>

        <div class="form-group col-xs-12 col-lg-12">

            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?= 
                $form->field($model, 'data_inicio')->label('Data de início do Semestre')->widget(DateControl::class,[
                    'widgetOptions' => [
                        'options' => [
                            'placeholder' => $comments['data_inicio']
                        ]
                    ],
                    'type' => DateControl::FORMAT_DATE,])
                ?>
            </div>

            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?= 
                $form->field($model, 'data_termino')->label('Data de término do Semestre')->widget(DateControl::class,[
                    'widgetOptions' => [
                        'options' => [
                            'placeholder' => $comments['data_termino']
                        ]
                    ],
                    'type' => DateControl::FORMAT_DATE,])
                ?>
            </div>

        </div>
        
        <!--PAD-->
        <h2 class="page-header"><i class="fa fa-edit"></i> PAD</h2>
        <div class="form-group col-xs-12 col-lg-12">
            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?= 
                $form->field($model, 'data_inicio_envio_pad')->widget(DateControl::class,[
                    'widgetOptions' => [
                        'options' => [
                            'placeholder' => $comments['data_inicio_envio_pad']
                        ]
                    ],
                    'type' => DateControl::FORMAT_DATE,])
                ?>
            </div>

            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?= 
                $form->field($model, 'data_termino_envio_pad')->widget(DateControl::class,[
                    'widgetOptions' => [
                        'options' => [
                            'placeholder' => $comments['data_termino_envio_pad']
                        ]
                    ],
                    'type' => DateControl::FORMAT_DATE,])
                ?>
            </div>
        </div>

        <div class="form-group col-xs-12 col-lg-12">
            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?= 
                $form->field($model, 'data_inicio_avaliacao_pad')->widget(DateControl::class,[
                    'widgetOptions' => [
                        'options' => [
                            'placeholder' => $comments['data_inicio_avaliacao_pad']
                        ]
                    ],
                    'type' => DateControl::FORMAT_DATE,])
                ?>
            </div>

            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?= 
                $form->field($model, 'data_termino_avaliacao_pad')->widget(DateControl::class,[
                    'widgetOptions' => [
                        'options' => [
                            'placeholder' => $comments['data_termino_avaliacao_pad']
                        ]
                    ],
                    'type' => DateControl::FORMAT_DATE,])
                ?>
            </div>
        </div>

        <div class="form-group col-xs-12 col-lg-12">
            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?= 
                $form->field($model, 'data_inicio_correcao_pad')->widget(DateControl::class,[
                    'widgetOptions' => [
                        'options' => [
                            'placeholder' => $comments['data_inicio_correcao_pad']
                        ]
                    ],
                    'type' => DateControl::FORMAT_DATE,])
                ?>
            </div>

            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?= 
                $form->field($model, 'data_termino_correcao_pad')->widget(DateControl::class,[
                    'widgetOptions' => [
                        'options' => [
                            'placeholder' => $comments['data_termino_correcao_pad']
                        ]
                    ],
                    'type' => DateControl::FORMAT_DATE,])
                ?>
            </div>
        </div>
        
        <h2 class="page-header"><i class="fa fa-edit"></i> RAD</h2>
        <div class="form-group col-xs-12 col-lg-12">
            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?= 
                $form->field($model, 'data_inicio_envio_rad')->widget(DateControl::class,[
                    'widgetOptions' => [
                        'options' => [
                            'placeholder' => $comments['data_inicio_envio_rad']
                        ]
                    ],
                    'type' => DateControl::FORMAT_DATE,])
                ?>
            </div>

            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?= 
                $form->field($model, 'data_termino_envio_rad')->widget(DateControl::class,[
                    'widgetOptions' => [
                        'options' => [
                            'placeholder' => $comments['data_termino_envio_rad']
                        ]
                    ],
                    'type' => DateControl::FORMAT_DATE,])
                ?>
            </div>
        </div>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
