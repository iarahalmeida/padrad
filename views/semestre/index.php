<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SemestreSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Semestres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="semestre-index">
    <!-- Default box -->
    <div class="box box-success">
        <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Criar semestre', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ano_numero',
            'data_inicio',
            'data_termino',
            //'data_inicio_envio_pad',
            //'data_termino_envio_pad',
            //'data_inicio_avaliacao_pad',
            //'data_termino_avaliacao_pad',
            //'data_inicio_correcao_pad',
            //'data_termino_correcao_pad',
            //'data_inicio_envio_rad',
            //'data_termino_envio_rad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
            <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
