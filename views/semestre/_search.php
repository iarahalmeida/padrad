<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SemestreSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="semestre-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ano_numero') ?>

    <?= $form->field($model, 'data_inicio') ?>

    <?= $form->field($model, 'data_termino') ?>

    <?= $form->field($model, 'data_inicio_envio_pad') ?>

    <?php // echo $form->field($model, 'data_termino_envio_pad') ?>

    <?php // echo $form->field($model, 'data_inicio_avaliacao_pad') ?>

    <?php // echo $form->field($model, 'data_termino_avaliacao_pad') ?>

    <?php // echo $form->field($model, 'data_inicio_correcao_pad') ?>

    <?php // echo $form->field($model, 'data_termino_correcao_pad') ?>

    <?php // echo $form->field($model, 'data_inicio_envio_rad') ?>

    <?php // echo $form->field($model, 'data_termino_envio_rad') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
