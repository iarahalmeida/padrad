<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AtividadePesquisa */

$this->title = 'Criar Atividade Pesquisa';
$this->params['breadcrumbs'][] = ['label' => 'Atividade de Pesquisa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atividade-pesquisa-create">

    <div class="box box-success">
        <div class="box-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
        </div>
    </div>

</div>
