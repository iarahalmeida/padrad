<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\DetalhesAtividade */
/* @var $form yii\widgets\ActiveForm */

//$comments = $model->attributeComments();
?>

<div class="detalhes-atividade-form">

    <?php $form = ActiveForm::begin(); ?>
    
        <h2 class="page-header"><i class="fa fa-edit"></i> Nome</h2>
        <div class="form-group col-xs-12 col-lg-12">

            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?=
                $form->field($model, 'nome')->textInput(['maxlength' => true])
                ?>
            </div>
            
            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?= 
                    $form->field($model, 'tipo')->dropDownList(
                        ['Pesquisa'=>'Pesquisa','Ensino'=>'Ensino','Extensão'=>'Extensão','Gestão'=>'Gestão','Outros'=>'Outros']
                    )
                ?>
            </div>
        
        </div>
       
        <h2 class="page-header"><i class="fa fa-clock-o"></i> Carga Horária</h2>
        <div class="form-group col-xs-12 col-lg-12">
            
            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?=
                $form->field($model, 'carga_horaria_minima')->textInput()
                ?>
            </div>
            
            <div class="col-xs-12 col-sm-3 col-lg-3">
                <?=
                $form->field($model, 'carga_horaria_maxima')->textInput()
                ?>
            </div>

        </div>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
