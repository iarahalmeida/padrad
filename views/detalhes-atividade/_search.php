<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DetalhesAtividadeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detalhes-atividade-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nome') ?>
    
    <?= $form->field($model, 'tipo') ?>

    <?= $form->field($model, 'carga_horaria_maxima') ?>

    <?= $form->field($model, 'carga_horaria_minima') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
