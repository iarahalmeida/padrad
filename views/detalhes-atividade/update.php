<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DetalhesAtividade */

$this->title = 'Editar Detalhes de Atividade: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Detalhes Atividades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="detalhes-atividade-update">

    <div class="box box-success">
        <div class="box-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

</div>
