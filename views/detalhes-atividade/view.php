<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DetalhesAtividade */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Detalhes Atividades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="detalhes-atividade-view">

    <div class="box box-success">
        <div class="box-body">

    <p>
        <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Excluir', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nome',
            'tipo',
            'carga_horaria_maxima',
            'carga_horaria_minima',
        ],
    ]) ?>
    
        </div>
        <!-- /.box-body -->
    </div>
</div>
