<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AtividadeEnsino */

$this->title = 'Criar atividade de ensino';
$this->params['breadcrumbs'][] = ['label' => 'Atividades de Ensino', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atividade-ensino-create">

      <!-- Default box -->
    <div class="box box-success">
        <div class="box-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
