<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtividadeEnsinoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Atividades de Ensino';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atividade-ensino-index">

      <!-- Default box -->
    <div class="box box-success">
        <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Criar Atividade de Ensino', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nome',
            'carga_horaria',
            'data_inicio',
            'data_fim',
            //'tipo',
            //'id_detalhesatividade',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
            </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
