<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AtividadeEnsino */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Atividade Ensinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="atividade-ensino-view">
      <!-- Default box -->
    <div class="box box-success">
        <div class="box-body">

    <p>
        <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Excluir', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Você tem certeza que deseja excluir este item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nome',
            'carga_horaria',
            'data_inicio',
            'data_fim',
            'tipo',
            'id_detalhesatividade',
            [
                'label' => 'Detalhes Atividade',
                'value' => $model->detalhesatividade->nome,
            ],
        ],
    ]) ?>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

</div>
