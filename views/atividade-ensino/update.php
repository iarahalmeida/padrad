<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AtividadeEnsino */

$this->title = 'Alterar atividade de ensino: ' . $model->nome . ' (' . $model->id . ')';
$this->params['breadcrumbs'][] = ['label' => 'Atividades de Ensino', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nome . ' (' . $model->id . ')', 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Alterar';
?>
<div class="atividade-ensino-update">

      <!-- Default box -->
    <div class="box box-success">
        <div class="box-body">
            <div class="user-form">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
