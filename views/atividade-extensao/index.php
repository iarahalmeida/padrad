<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtividadeExtensaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Atividades de Extensão';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atividade-extensao-index">

    <div class="box box-success">
        <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Criar Atividade de Extensão', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nome',
            'carga_horaria',
            'data_inicio',
            'data_fim',
            //'arquivo',
            //'id_detalhesatividade',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
    </div>
</div>
