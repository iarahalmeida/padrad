<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AtividadeExtensao */

$this->title = 'Criar Atividade de Extensão';
$this->params['breadcrumbs'][] = ['label' => 'Atividade de Extensão', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atividade-extensao-create">

    <div class="box box-success">
        <div class="box-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

        </div>
    </div>
</div>
