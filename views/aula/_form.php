<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Disciplina;
use app\models\Semestre;
use app\models\Curso;


/* @var $this yii\web\View */
/* @var $model app\models\Aula */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aula-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $curso = ArrayHelper::map(Curso::find()->all(), 'id', 'nome'); ?>
    <?= $form->field($model, 'id_curso')->dropDownList($curso, ['prompt' => ''])->label('ID CURSO') ?>
    
    <?php $disciplina = ArrayHelper::map(Disciplina::find()->all(), 'id', 'nome'); ?>
    
    <?= $form->field($model, 'id_disciplina')->dropDownList($disciplina, ['prompt' => ''])->label('Disciplina') ?>
    
    <?php $semestre = ArrayHelper::map(Semestre::find()->all(), 'id', 'ano_numero'); ?>
    
    <?= $form->field($model, 'id_semestre')->dropDownList($semestre, ['prompt' => ''])->label('Semestre') ?>
    
    <?= $form->field($model, 'id_user')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
