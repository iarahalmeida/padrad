<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Semestre;

/* @var $this \yii\web\View */
/* @var $model app\models\HomeForm */
/* @var $form yii\widgets\ActiveForm */


app\assets\AppAsset::register($this);

dmstr\web\AdminLteAsset::register($this);

$this->title = 'Home';
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <?php $this->head() ?>
    </head>
<body class="hold-transition skin-green layout-top-nav">
<?php $this->beginBody() ?>

<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="../web" class="navbar-brand"><b>PAD/RAD</b> IFNMG</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li><a href="index.php?r=site%2Flogin">Login</a></li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Buscar PAD/RAD</h3>
          </div>
          <div class="box-body">
          
        <?php
            $form = ActiveForm::begin([
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
            ]);
        ?>

        <?= $form->field($model, 'professor_name')->textInput(['maxlength' => true]) ?>
    
        <?php $semester = ArrayHelper::map(Semestre::find()->all(), 'id', 'ano_numero'); ?>
    
        <?= $form->field($model, 'semester_id')->dropDownList($semester, ['prompt' => ''])->label('Semestre') ?>

        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-search"></i> Buscar', ['class' => 'btn btn-success']) ?>
            <?= Html::submitButton('<i class="fa fa-download"></i> Gerar PDF', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Versão</b> alpha
      </div>
      <strong>Copyright &copy; 2018-2019 Engenharia de Software II.</strong> Turma 2018/2.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<?php ?>
