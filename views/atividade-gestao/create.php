<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AtividadeGestao */

$this->title = 'Create Atividade Gestao';
$this->params['breadcrumbs'][] = ['label' => 'Atividade Gestaos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atividade-gestao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
