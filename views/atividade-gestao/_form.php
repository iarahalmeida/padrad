<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;
use app\models\DetalhesAtividade;

/* @var $this yii\web\View */
/* @var $model app\models\AtividadeGestao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="atividade-gestao-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carga_horaria')->textInput() ?>

    <?= 
    $form->field($model, 'data_inicio')->label('Data de início da Atividade')->widget(DateControl::class,[
        'widgetOptions' => [
            'options' => [
                
            ]
        ],
    'type' => DateControl::FORMAT_DATE,])
    ?>

    <?= 
    $form->field($model, 'data_fim')->label('Data de término da Atividade')->widget(DateControl::class,[
        'widgetOptions' => [
            'options' => [
                
            ]
        ],
    'type' => DateControl::FORMAT_DATE,])
    ?>

    <?= $form->field($model, 'numero_portaria')->textInput(['maxlength' => true]) ?>

    <?php $detalhesatividade = ArrayHelper::map(DetalhesAtividade::find()->all(), 'id', 'nome'); ?>
    
    <?= $form->field($model, 'id_detalhesatividade')->dropDownList($detalhesatividade, ['prompt' => ''])->label('Detalhes de Atividade') ?>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
