<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtividadeGestaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Atividade Gestaos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atividade-gestao-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Atividade Gestao', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nome',
            'carga_horaria',
            'data_inicio',
            'data_fim',
            //'numero_portaria',
            //'id_detalhesatividade',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
